package ru.kdv.Animals;

/**
 * Created by karab on 01.02.2017.
 */
public class Ket extends Animals {

    public Ket() {
        this("кот", "мау-мяу");
    }


    protected Ket(String name, String voice) {
        super(name, voice);
    }
}
