package ru.kdv.Animals;

/**
 * Created by karab on 01.02.2017.
 * реализ кот
 */
public class Main {

    public static void main(String... args) {
        Dog dog = new Dog();
        Dog dog1 = new Dog("Собака", "гав-гав");
        Dog dog2 = new Dog("Тузик", "тяф-тяф");
        Ket ket = new Ket();
        Ket ket1 = new Ket("Рыжик", "мау-мяу");
        Parrot parrot=new Parrot();
        Parrot parrot1=new Parrot("кеша","чирик-чирик");
        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();
        ket.printDisplay();
        ket1.printDisplay();
        parrot.printDisplay();
        parrot1.printDisplay();

    }
}

