package ru.kdv.Animals;

/**
 * Created by karab on 01.02.2017.
 */
public class Parrot extends Animals {

    public Parrot() {
        this("попугай", "чирик-чирик");
    }


    protected Parrot(String name, String voice) {
        super(name, voice);
    }
}

